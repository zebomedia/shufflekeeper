//
//  FrameScoreTests.swift
//  ShuffleKeeperTests
//
//  Created by Eric Zelermyer on 11/26/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import XCTest
@testable import ShuffleKeeper

class FrameScoreTests: XCTestCase {

    override func setUp() {
        let team1 = Team(name: "RBF")
        let team2 = Team(name: "Randos")
        
        let shot = Shot(team: team1,
                        shotType: .leftSeven,
                        teamType: .first)
        
        let frameModel = FrameScoreViewModel(teams: [team1, team2],
                                             frame: Frame(num: 1,
                                                          shots: [shot]),
                                             firstTeamIndex: 0)
        let modal = FrameScoreViewController(viewModel: frameModel)

        let controller = UIViewController()
        UIApplication.shared.windows.first!.rootViewController = controller
        
        let nav = UINavigationController(rootViewController: modal)
        controller.view.layoutSubviews()
        
        controller.present(nav, animated: true, completion: nil)
    }

    override func tearDown() {
    }

    func testView() {
        let expect = expectation(description: "display view")
        
        wait(for: [expect], timeout: 999)
    }
}
