//
//  GlobalConstants.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/12/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

enum GlobalConstants {
    static let lineWidth: CGFloat = 1.0
}
