//
//  ShotViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/22/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct ShotViewModel {
    let image: UIImage
    let rect: CGRect
}
