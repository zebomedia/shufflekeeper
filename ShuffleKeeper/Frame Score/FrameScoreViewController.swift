//
//  FrameScoreViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class FrameScoreViewController: UIViewController {
    let viewModel: FrameScoreViewModel
    
    var frameScoreView: FrameScoreView {
        return view as! FrameScoreView
    }
    
    init(viewModel: FrameScoreViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .cancel,
                                                        target: self,
                                                        action: #selector(cancel)),
                                        animated: false)
    }
    
    override func loadView() {
        view = FrameScoreView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.title
        
        frameScoreView.teamChooser.setTitle(viewModel.teamNames[0], forSegmentAt: 0)
        frameScoreView.teamChooser.setTitle(viewModel.teamNames[1], forSegmentAt: 1)
        frameScoreView.teamChooser.addTarget(self, action: #selector(selectTeam), for: .valueChanged)
        
        frameScoreView.court.tapHandler = { [unowned self] point in
            self.viewModel.takeShot(point: point)
        }
        
        frameScoreView.court.tapBiscuitHandler = { [unowned self] view, index in
            view.removeFromSuperview()
            self.viewModel.removeShot(at: index)
        }

        viewModel.addBiscuitHandler = { [unowned self] biscuitImage, rect in
            self.frameScoreView.court.addBiscuit(image: biscuitImage, frame: rect, teamNumber: self.viewModel.selectedTeamIndex)
        }
        
        viewModel.adjustBiscuitsHandler = { [unowned self] rects in
            self.adjustBiscuits(rects: rects)
        }
        
        viewModel.updateScoreHandler = { [unowned self] score1, score2 in
            self.updateScores(score1: score1, score2: score2)
        }
        
        viewModel.changeTeamHandler = { [unowned self] index in
            self.frameScoreView.teamChooser.selectedSegmentIndex = index
        }
        
        frameScoreView.stackView.sizingHandler = { [unowned self] in
            self.viewModel.setCourtBounds(rect: self.frameScoreView.court.bounds)

            self.setupLabels()
            self.addBiscuits(for: self.viewModel.shotModels)
            self.frameScoreView.adjustCirclePositions()
        }
        
        frameScoreView.saveButton.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        frameScoreView.team1Score.text = viewModel.scoreForTeam(at: 0)
        frameScoreView.team2Score.text = viewModel.scoreForTeam(at: 1)
        
        frameScoreView.teamChooser.selectedSegmentIndex = viewModel.selectedTeamIndex
    }
    
    private func setupLabels() {
        for shotType in ShotType.allCases {
            let frame = viewModel.labelFrame(for: shotType)
            frameScoreView.court.addScoreLabel(frame: frame, text: shotType.labelText)
        }
    }
    
    @objc
    private func selectTeam() {
        viewModel.selectTeam(at: frameScoreView.teamChooser.selectedSegmentIndex)
    }
    
    private func addBiscuits(for shotModels: [ShotViewModel]) {
        for model in shotModels {
            frameScoreView.court.addBiscuit(image: model.image,
                                            frame: model.rect,
                                            teamNumber: nil)
        }
    }
    
    private func adjustBiscuits(rects: [CGRect]) {
        for (index, rect) in rects.enumerated() {
            frameScoreView.court.moveBiscuit(at: index, to: rect)
        }
    }
    
//    @objc
//    private func panBiscuit(gesture: UIPanGestureRecognizer) {
//        guard let biscuit = gesture.view else { return }
//        let position = biscuit.center
//
//        let translation = gesture.translation(in: biscuit.superview)
//        let velocity = gesture.velocity(in: frameScoreView)
//
//        biscuit.center = CGPoint(x: position.x + translation.x, y: position.y + translation.y)
//    }
    
    private func updateScores(score1: String, score2: String) {
        frameScoreView.team1Score.text = score1
        frameScoreView.team2Score.text = score2
    }
    
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func save() {
        viewModel.save()
        dismiss(animated: true, completion: nil)
    }
}
