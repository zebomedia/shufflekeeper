//
//  CourtView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/21/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class CourtView: UIView {
    var tapHandler: ((CGPoint) -> Void)?
    var tapBiscuitHandler: ((UIView, Int) -> Void)?
    
    private var labels: [UILabel] = []
    
    private(set) lazy var court: UIImageView = self.configureSubview(UIImageView(image: UIImage(named: "Court")!), useConstraints: false) {
        $0.tintColor = UIColor.systemGray2
        $0.isUserInteractionEnabled = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(courtTapped(gesture:)))
        court.addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        court.frame = bounds
    }
    
    @objc
    private func courtTapped(gesture: UITapGestureRecognizer) {
        let point = gesture.location(in: court)
        
        tapHandler?(point)
    }

    func addScoreLabel(frame: CGRect, text: String) {
        let label = self.court.configureSubview(UILabel(), useConstraints: false) {
            $0.font = UIFont.preferredFont(forTextStyle: .largeTitle)
            $0.textAlignment = .center
            $0.textColor = UIColor.systemGray2
        }
        label.frame = frame
        label.text = text
    }
    
    func biscuitIndex(for biscuit: UIView) -> Int? {
        return subviews.firstIndex(of: biscuit).map { $0 - 1 }
    }
    
    func biscuit(at index: Int) -> UIView {
        return subviews[index + 1]
    }
    
    func addBiscuit(image: UIImage, frame: CGRect, teamNumber: Int?) {
        let imageView = UIImageView(image: image)
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .center

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBiscuit(gesture:)))
        imageView.addGestureRecognizer(tapGesture)
        
        addSubview(imageView)
        
        guard let teamNumber = teamNumber else {
            imageView.frame = frame
            return
        }
        
        let w = bounds.width
        let startRange = teamNumber == 0 ? frame.width..<w/2-frame.width*2 : w/2+frame.width..<w-frame.width*2
        let startX = CGFloat.random(in: startRange)
        let startY = -frame.height
        
        imageView.frame = CGRect(x: startX, y: startY, width: frame.width, height: frame.height)
        
        let percentage = Double(frame.midY/bounds.height)
        let duration: TimeInterval = max(0.15, percentage * 0.5)
        let animator = UIViewPropertyAnimator(duration: duration, curve: .easeOut) {
            imageView.frame = frame
        }
        
        animator.startAnimation()
    }
    
    func moveBiscuit(at index: Int, to frame: CGRect) {
        let view = biscuit(at: index)
        
        let animator = UIViewPropertyAnimator(duration: 0.15, curve: .easeInOut) {
            view.frame = frame
        }
        
        animator.startAnimation()
    }
    
    @objc
    private func tapBiscuit(gesture: UITapGestureRecognizer) {
        guard let biscuit = gesture.view, let index = biscuitIndex(for: biscuit) else { return }
        
        tapBiscuitHandler?(biscuit, index)
    }
    

}
