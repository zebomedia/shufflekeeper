//
//  FrameScoreView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class FrameScoreView: UIView {
    enum Constants {
        static let sideMargin: CGFloat = 40
        static let topMargin: CGFloat = 20
        static let bottomMargin: CGFloat = 20
        static let spacing: CGFloat = 20
        static let circleSize: CGFloat = 14
        
        static let chooserFont = UIFont.preferredFont(forTextStyle: .subheadline)
    }
    
    private var yellowCircleConstraint: NSLayoutConstraint!
    private var blackCircleConstraint: NSLayoutConstraint!
    
    private(set) lazy var stackView: SizingStackView = self.configureSubview(SizingStackView(arrangedSubviews: [self.teamChooser,
                                                                                                   self.scoreStackView,
                                                                                                   self.court,
                                                                                                   self.saveButton]))
    {
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = Constants.spacing
        $0.isLayoutMarginsRelativeArrangement = true
        $0.layoutMargins = UIEdgeInsets(top: Constants.topMargin,
                                        left: Constants.sideMargin,
                                        bottom: Constants.bottomMargin,
                                        right: Constants.sideMargin)
        $0.setCustomSpacing(12, after: self.scoreStackView)
        $0.setCustomSpacing(40, after: self.court)
    }
    
    private(set) lazy var teamChooser: UISegmentedControl = self.configureSubview(UISegmentedControl(items: ["Team 1", "Team 2"])) {
        $0.selectedSegmentIndex = 0
        $0.heightAnchor.constraint(equalToConstant: 36).activate()
        let baseFont = UIFont.preferredFont(forTextStyle: .body)
        $0.setTitleTextAttributes([.font: Constants.chooserFont], for: .normal)
        $0.setTitleTextAttributes([.font: Constants.chooserFont.bold()], for: .selected)
    }
    
    private lazy var scoreStackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.team1Score, self.team2Score])) {
        $0.axis = .horizontal
        $0.distribution = .fillEqually
    }
    
    private(set) lazy var team1Score: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .headline)
        $0.textAlignment = .center
        $0.text = "0"
    }
    
    private(set) lazy var team2Score: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .headline)
        $0.textAlignment = .center
        $0.text = "0"
    }
    
    private(set) lazy var court: CourtView = self.configureSubview(CourtView()) {
        $0.setContentHuggingPriority(.defaultLow, for: .vertical)
    }
    
    private(set) lazy var saveButton: UIButton = self.configureSubview(UIButton.solidButton(title: "Save Frame"))
    
    private lazy var yellowCircle: CircleView = self.configureSubview(CircleView()) {
        $0.color = UIColor.ShuffleKeeper.yellow
        $0.widthAnchor.constraint(equalToConstant: Constants.circleSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.circleSize).activate()
    }
    
    private lazy var blackCircle: CircleView = self.configureSubview(CircleView()) {
        $0.color = UIColor.ShuffleKeeper.black
        $0.widthAnchor.constraint(equalToConstant: Constants.circleSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.circleSize).activate()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.ShuffleKeeper.backgroundColor
        
        safeAreaLayoutGuide.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).activate()
        safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).activate()
        safeAreaLayoutGuide.topAnchor.constraint(equalTo: stackView.topAnchor).activate()
        safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).activate()
        
        yellowCircleConstraint = yellowCircle.centerXAnchor.constraint(equalTo: teamChooser.centerXAnchor).activate()
        blackCircleConstraint = blackCircle.centerXAnchor.constraint(equalTo: teamChooser.centerXAnchor).activate()
        yellowCircle.centerYAnchor.constraint(equalTo: teamChooser.centerYAnchor, constant: 1).activate()
        blackCircle.centerYAnchor.constraint(equalTo: teamChooser.centerYAnchor, constant: 1).activate()
    }
    
    func adjustCirclePositions() {
        let segmentWidth = teamChooser.bounds.width/2
        
        let name1Width = Constants.chooserFont.widthOfString(teamChooser.titleForSegment(at: 0)! as NSString)
        let name2Width = Constants.chooserFont.widthOfString(teamChooser.titleForSegment(at: 1)! as NSString)
        
        yellowCircleConstraint.constant = -(segmentWidth/2 + name1Width/2 + Constants.circleSize/2 + 6)
        blackCircleConstraint.constant = segmentWidth/2 - name2Width/2 - Constants.circleSize/2 - 6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
