//
//  FrameScoreViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class FrameScoreViewModel {
    enum Constants {
        static let biscuitSize: CGFloat = 40
    }
    
    var addBiscuitHandler: ((UIImage, CGRect) -> Void)?
    var adjustBiscuitsHandler: (([CGRect]) -> Void)?
    var updateScoreHandler: ((String, String) -> Void)?
    var saveHandler: ((Frame) -> Void)?
    var changeTeamHandler: ((Int) -> Void)?
    
    var title: String {
        return "Frame \(frame.num + 1)"
    }
    
    var teamNames: [String] {
        return teams.map { $0.name }
    }
    
    var shotModels: [ShotViewModel] {
        var shotRects = court.getRects(for: frame.shots, biscuitSize: Constants.biscuitSize)
        return frame.shots.map { ShotViewModel(image: $0.biscuit, rect: shotRects.removeFirst())}
    }
    
    private var selectedTeam: Team {
        return teams[selectedTeamIndex]
    }
    
    private let teams: [Team]
    private(set) var selectedTeamIndex: Int
    private var frame: Frame
    private var court = CourtGeometry(bounds: CGRect.zero)
    
    init(teams: [Team], frame: Frame, firstTeamIndex: Int) {
        self.frame = frame
        self.teams = teams
        self.selectedTeamIndex = firstTeamIndex
    }
    
    func setCourtBounds(rect: CGRect) {
        court = CourtGeometry(bounds: rect)
    }
    
    func takeShot(point: CGPoint) {
        guard court.isOnCourt(point: point) else { return }
        
        let teamType = TeamType(rawValue: selectedTeamIndex)!
        
        guard frame.shotCount(for: teamType) < 4 else { return }
        
        let shotType = court.getShotType(point: point)
        
        let shot = Shot(team: selectedTeam, shotType: shotType, teamType: teamType)
        frame.shots.append(shot)
        
        var shotRects = court.getRects(for: frame.shots, biscuitSize: Constants.biscuitSize)
        guard !shotRects.isEmpty else { return }
        
        addBiscuitHandler?(shot.biscuit, shotRects.popLast()!)
        
        if !shotRects.isEmpty {
            adjustBiscuitsHandler?(shotRects)
        }
        
        updateScore()
        
        if frame.shotCount(for: teamType) == 4 {
            selectedTeamIndex = (selectedTeamIndex == 0) ? 1 : 0
            changeTeamHandler?(selectedTeamIndex)
        }
    }
    
    func removeShot(at index: Int) {
        frame.shots.remove(at: index)
        
        updateScore()

        let shotRects = court.getRects(for: frame.shots, biscuitSize: Constants.biscuitSize)
        guard !shotRects.isEmpty else { return }
        
        adjustBiscuitsHandler?(shotRects)
    }
    
    func selectTeam(at index: Int) {
        selectedTeamIndex = index
    }
    
    func labelFrame(for shotType: ShotType) -> CGRect {
        let center = court.centerPoint(for: shotType)
        let w: CGFloat = 100
        let h: CGFloat = 40
        
        return CGRect(x: center.x-w/2, y: center.y-h/2, width: w, height: h).integral
    }
    
    func scoreForTeam(at index: Int) -> String {
        return "\(frame.score(for: teams[index]))"
    }
    
    func save() {
        saveHandler?(frame)
    }
    
    private func updateScore() {
        updateScoreHandler?("\(scoreForTeam(at: 0))", "\(scoreForTeam(at: 1))")
    }
}
