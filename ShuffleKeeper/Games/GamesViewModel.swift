//
//  GamesViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GamesViewModel {
    enum Constants {
        static let winnerFont = UIFont.preferredFont(forTextStyle: .body).bold()
        static let loserFont = UIFont.preferredFont(forTextStyle: .body)
        static let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            return formatter
        }()
        static let rowHeight: CGFloat = 110
        static let footerHeight: CGFloat = 100
        static let filename: String = "games.json"
    }
    
    var reloadHandler: (() -> Void)?
    var addHandler: ((Int) -> Void)?
    var removeHandler: ((Int) -> Void)?
    
    var navigationHandler: NavigationHandler?
    var footerHeight: CGFloat {
        return Constants.footerHeight
    }
    
    var showEmptyMessage: Bool {
        return games.isEmpty
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .none
        formatter.dateStyle = .short
        return formatter
    }()
    
//    private var games: [Game] = [Game(date: Date(), teams: [Team(name: "RBF"), Team(name: "Pootie Tang")], frames: [])]
    private(set) var games: [Game] = []

    private var selectedGameIndex: Int?
    
    init() {
        games = loadGames()
    }
    
    func createGame() {
        navigationHandler?(NavigationCommand.newGame)
    }
    
    func addGame(team1: String, team2: String) {
        let game = Game(date: Date(),
                        teams: [Team(name: team1),
                                Team(name: team2)],
                        frames: [])
        games.append(game)
        
        addHandler?(games.count-1)
        
        navigationHandler?(NavigationCommand.showGame(game: game))
        
        persistGames()
    }
    
    var numberOfGames: Int {
        return games.count
    }
    
    func cellModel(at indexPath: IndexPath) -> GameCellViewModel {
        let game = games[indexPath.row]
        
        let score1 = game.totalScore(forTeamAt: 0)
        let score2 = game.totalScore(forTeamAt: 1)
        
        let winner: Int? = {
            guard score1 != score2 else { return nil }
            return score1 > score2 ? 0 : 1
        }()
        
        let font1 = winner == 0 ? Constants.winnerFont : Constants.loserFont
        let font2 = winner == 1 ? Constants.winnerFont : Constants.loserFont
        
        let team1 = GameCellViewModel.Team(color: UIColor.ShuffleKeeper.yellow,
                                           name: game.teams[0].name,
                                           score: "\(score1)",
            font: font1)
        
        let team2 = GameCellViewModel.Team(color: UIColor.ShuffleKeeper.black,
                                           name: game.teams[1].name,
                                           score: "\(score2)",
            font: font2)
        
        return GameCellViewModel(team1: team1, team2: team2)
    }
    
    func height(forRowAt index: Int) -> CGFloat {
        return Constants.rowHeight
    }
    
    func selectGame(at index: Int) {
        selectedGameIndex = index
        
        let game = games[index]
        
        navigationHandler?(NavigationCommand.showGame(game: game))
    }
    
    func update(game: Game) {
        guard let index = selectedGameIndex else { return }
        games[index] = game
        
        reloadHandler?()
        
        persistGames()
    }
    
    func remove(gameAt index: Int) {
        games.remove(at: index)
        
        removeHandler?(index)
        
        persistGames()
    }
    
    private func loadGames() -> [Game] {
        do {
            return try StorageHelper.retrieve(Constants.filename, from: .documents, as: [Game].self)
        }
        catch(let error) {
            print("Error retrieving games: \(error.localizedDescription)")
            return []
        }
    }
    
    private func persistGames() {
        do {
            try StorageHelper.store(games, to: .documents, as: Constants.filename)
        }
        catch(let error) {
            print("Error saving games: \(error.localizedDescription)")
        }
    }
}
