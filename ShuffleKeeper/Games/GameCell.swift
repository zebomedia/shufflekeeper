//
//  GameCell.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameCell: UITableViewCell {
    enum Constants {
        static let leftMargin: CGFloat = 30
        static let rightMargin: CGFloat = 50
        static let topMargin: CGFloat = 16
        static let bottomMargin: CGFloat = 16
    }
    
    private lazy var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.team1Details, self.team2Details])) {
        $0.axis = .vertical
        $0.spacing = 0
        $0.distribution = .fillEqually
        $0.isLayoutMarginsRelativeArrangement = true
        $0.layoutMargins = UIEdgeInsets(top: Constants.topMargin, left: Constants.leftMargin, bottom: Constants.bottomMargin, right: Constants.rightMargin)
    }
    
    private lazy var team1Details = self.configureSubview(GameTeamScoreView())
    private lazy var team2Details = self.configureSubview(GameTeamScoreView())
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        constrainSubview(stackView, insets: .zero)
        
        accessoryType = .disclosureIndicator
        backgroundColor = UIColor.ShuffleKeeper.backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GameCell {
    func configure(with viewModel: GameCellViewModel) {
        team1Details.configure(with: viewModel.team1)
        team2Details.configure(with: viewModel.team2)
    }
}
