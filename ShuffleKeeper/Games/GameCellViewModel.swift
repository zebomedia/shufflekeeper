//
//  GameCellViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct GameCellViewModel {
    struct Team {
        let color: UIColor
        let name: String
        let score: String
        let font: UIFont
    }
    
    let team1: Team
    let team2: Team
}
