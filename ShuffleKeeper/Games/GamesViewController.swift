//
//  GamesViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GamesViewController: UIViewController {
    let viewModel: GamesViewModel
    
    var gamesView: GamesView {
        return view as! GamesView
    }
    
    var tableView: UITableView {
        return gamesView.tableView
    }
    
    init(viewModel: GamesViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = GamesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Games"
        tableView.backgroundColor = UIColor.ShuffleKeeper.backgroundColor
        
        tableView.tableFooterView = UIView()
        tableView.register(cellType: GameCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        
        gamesView.footer.button.addTarget(self, action: #selector(addGame), for: .touchUpInside)
        
        viewModel.reloadHandler = { [weak self] in
            self?.tableView.reloadData()
            self?.checkEmptyState()
        }
        
        viewModel.addHandler = { [weak self] index in
            self?.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            self?.checkEmptyState()
        }
        
        viewModel.removeHandler = { [weak self] index in
            self?.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            self?.checkEmptyState()
        }
        
        checkEmptyState()
    }
    
    @objc func addGame() {
        viewModel.createGame()
    }
    
    private func showRemoveAlert(index: Int) {
        let alert = UIAlertController(title: "Remove Game", message: "Are you sure you want to remove this game?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { [weak self] _ in
            self?.viewModel.remove(gameAt: index)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    private func checkEmptyState() {
        gamesView.emptyMessage.isHidden = !viewModel.showEmptyMessage
    }
}

extension GamesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfGames
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.height(forRowAt: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.height(forRowAt: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GameCell = tableView.dequeueReusableCell(for: indexPath)
        
        let model = viewModel.cellModel(at: indexPath)
        
        cell.configure(with: model)
        
        return cell
    }
}
 
extension GamesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectGame(at: indexPath.row)
        tableView.selectRow(at: nil, animated: true, scrollPosition: .none)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Remove") { [weak self] (_, _, completion) in
            completion(true)
            self?.showRemoveAlert(index: indexPath.row)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}
