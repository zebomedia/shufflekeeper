//
//  GameTeamScoreView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/4/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class GameTeamScoreView: UIStackView {
    enum Constants {
        static let dotSize: CGFloat = 12
        static let spacing: CGFloat = 8
        static let nameWidth: CGFloat = 130
    }
    
    private(set) lazy var dot: CircleView = self.configureSubview(CircleView()) {
        $0.widthAnchor.constraint(equalToConstant: Constants.dotSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.dotSize).activate()
    }
    
    private(set) lazy var nameLabel: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .body)
        $0.widthAnchor.constraint(equalToConstant: Constants.nameWidth).activate()
    }
    
    private(set) lazy var scoreLabel: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .body)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        axis = .horizontal
        alignment = .center
        spacing = Constants.spacing
        
        addArrangedSubview(dot)
        addArrangedSubview(nameLabel)
        addArrangedSubview(scoreLabel)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GameTeamScoreView {
    func configure(with viewModel: GameCellViewModel.Team) {
        dot.color = viewModel.color
        nameLabel.text = viewModel.name
        scoreLabel.text = viewModel.score
        nameLabel.font = viewModel.font
        scoreLabel.font = viewModel.font
    }
}
