//
//  GamesView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/4/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class GamesView: UIView {
    enum Constants {
        static let messageWidth: CGFloat = 220
    }
    
    private(set) lazy var tableView: UITableView = self.configureSubview(UITableView())
    
    private(set) lazy var divider: UIView = self.configureSubview(UIView()) {
        $0.heightAnchor.constraint(equalToConstant: GlobalConstants.lineWidth).activate()
        $0.backgroundColor = UIColor.opaqueSeparator
    }
    
    private(set) lazy var footer: GameFooterView = self.configureSubview(GameFooterView()) {
        $0.button.setTitle("New Game", for: .normal)
        $0.heightAnchor.constraint(equalToConstant: GamesViewModel.Constants.footerHeight).activate()
        $0.backgroundColor = UIColor.ShuffleKeeper.backgroundColor
    }
    
    private(set) lazy var emptyMessage: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .headline)
        $0.widthAnchor.constraint(equalToConstant: Constants.messageWidth).activate()
        $0.isHidden = true
        $0.numberOfLines = 0
        $0.textAlignment = .center
        $0.textColor = UIColor.secondaryLabel
        $0.text = "No games yet! \rTap New Game below to fire up some biscuits."
    }
    
    private lazy var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.tableView, self.divider, self.footer])) {
        $0.axis = .vertical
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = UIColor.ShuffleKeeper.backgroundColor
        
        leadingAnchor.constraint(equalTo: stackView.leadingAnchor).activate()
        trailingAnchor.constraint(equalTo: stackView.trailingAnchor).activate()
        safeAreaLayoutGuide.topAnchor.constraint(equalTo: stackView.topAnchor).activate()
        safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).activate()
        
        emptyMessage.centerXAnchor.constraint(equalTo: centerXAnchor).activate()
        emptyMessage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -60).activate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
