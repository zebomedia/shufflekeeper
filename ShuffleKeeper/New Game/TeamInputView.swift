//
//  TeamInputView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/12/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

class TeamInputView: UIStackView {
    enum Constants {
        static let circleSize: CGFloat = 18
        static let spacing: CGFloat = 10
        static let inputWidth: CGFloat = 280
        static let inputHeight: CGFloat = 40
        static let cornerRadius: CGFloat = 8
    }

    lazy var circle: CircleView = self.configureSubview(CircleView()) {
        $0.widthAnchor.constraint(equalToConstant: Constants.circleSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.circleSize).activate()
        $0.color = self.color
    }

    lazy var input: InputField = self.configureSubview(InputField()) {
        $0.placeholder = self.placeholderText
        $0.widthAnchor.constraint(equalToConstant: Constants.inputWidth).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.inputHeight).activate()
    }
    
    let color: UIColor
    let placeholderText: String
    
    var delegate: UITextFieldDelegate? {
        get {
            return input.field.delegate
        }
        set {
            input.field.delegate = newValue
        }
    }
    
    init(color: UIColor, placeholderText: String) {
        self.color = color
        self.placeholderText = placeholderText

        super.init(frame: .zero)
        
        spacing = Constants.spacing
        alignment = .center
        
        addArrangedSubview(circle)
        addArrangedSubview(input)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
