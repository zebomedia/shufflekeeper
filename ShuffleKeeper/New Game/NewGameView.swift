//
//  NewGameView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/12/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class NewGameView: UIView {
    enum Constants {
        static let spacing: CGFloat = 20
        static let buttonMargin: CGFloat = 40
    }
    
    private(set) var centerConstraint: NSLayoutConstraint!
    
    lazy private(set) var team1Input: TeamInputView = self.configureSubview(TeamInputView(color: UIColor.ShuffleKeeper.yellow, placeholderText: "Team 1"))
    
    lazy private(set) var swapButton: UIButton = self.configureSubview(UIButton(type: .system)) {
        $0.setImage(UIImage(systemName: "arrow.up.arrow.down"), for: .normal)
        $0.tintColor = UIColor.secondaryLabel
    }
    
    lazy private(set) var team2Input: TeamInputView = self.configureSubview(TeamInputView(color: UIColor.ShuffleKeeper.black, placeholderText: "Team 2")) {
        $0.input.field.returnKeyType = .done
    }
    
    lazy private(set) var coinButton: UIButton = self.configureSubview(UIButton(type: .system)) {
        $0.setTitle("Coin Flip", for: .normal)
        $0.tintColor = UIColor.secondaryLabel
        $0.titleLabel?.textColor = UIColor.secondaryLabel
    }
    
    lazy private(set) var startButton: UIButton = self.configureSubview(UIButton.solidButton(title: "Start Game")) {
        $0.isEnabled = false
        $0.alpha = 0.5
    }
    
    lazy private var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.team1Input, self.swapButton, self.team2Input, self.coinButton])) {
        $0.axis = .vertical
        $0.spacing = Constants.spacing
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(backgroundTap))
        addGestureRecognizer(gesture)

        backgroundColor = UIColor.ShuffleKeeper.secondaryBackgroundColor
        
        centerConstraint = stackView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -70).activate()
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).activate()
        
        startButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.buttonMargin).activate()
        startButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.buttonMargin).activate()
        startButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -Constants.buttonMargin).activate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func backgroundTap() {
        endEditing(true)
    }
}
