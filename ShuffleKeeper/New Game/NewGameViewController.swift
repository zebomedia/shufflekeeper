//
//  NewGameViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/12/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class NewGameViewController: UIViewController {
    let viewModel: NewGameViewModel
    
    var newGameView: NewGameView {
        return view as! NewGameView
    }
    
    init(viewModel: NewGameViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        title = "New Game"

        navigationItem.setLeftBarButton(UIBarButtonItem(barButtonSystemItem: .cancel,
                                                        target: self,
                                                        action: #selector(cancel)),
                                        animated: false)
        
        newGameView.team1Input.delegate = self
        newGameView.team2Input.delegate = self
        
        newGameView.startButton.addTarget(self, action: #selector(startGame), for: .touchUpInside)
        newGameView.swapButton.addTarget(self, action: #selector(swapTeams), for: .touchUpInside)
        newGameView.coinButton.addTarget(self, action: #selector(flipCoin), for: .touchUpInside)
        
        newGameView.team1Input.input.moreButton.isHidden = viewModel.hideTeamChooser
        newGameView.team2Input.input.moreButton.isHidden = viewModel.hideTeamChooser
        
        newGameView.team1Input.input.textChangeHandler = { [unowned self] in self.textChanged() }
        newGameView.team2Input.input.textChangeHandler = { [unowned self] in self.textChanged() }
        
        newGameView.team1Input.input.moreHandler = { [unowned self] in
            self.showTeamMenu(from: self.newGameView.team1Input.input)
        }

        newGameView.team2Input.input.moreHandler = { [unowned self] in
            self.showTeamMenu(from: self.newGameView.team2Input.input)
        }
        
        viewModel.updateHandler = { [unowned self] in
            self.update()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = NewGameView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc private func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func startGame() {
        dismiss(animated: true) {
            self.viewModel.startGame(team1Name: self.newGameView.team1Input.input.text,
                                     team2Name: self.newGameView.team2Input.input.text)
        }
    }
    
    @objc private func flipCoin() {
        let flipper = CoinFlipViewController()
        flipper.modalPresentationStyle = .fullScreen
        present(flipper, animated: true, completion: nil)
    }
    
    @objc private func swapTeams() {
        let team1 = newGameView.team1Input.input.text
        newGameView.team1Input.input.text = newGameView.team2Input.input.text
        newGameView.team2Input.input.text = team1
    }
    
    private func textChanged() {
        viewModel.setTeamNames([newGameView.team1Input.input.text,
                                newGameView.team2Input.input.text])
    }
    
    private func update() {
        let enabled = self.viewModel.teamsAreValid
        newGameView.startButton.isEnabled = enabled
        newGameView.startButton.alpha = enabled ? 1.0 : 0.5
    }
    
    private func showTeamMenu(from input: InputField) {
        let menu = MenuTableViewController(items: viewModel.storedTeamNames) { item, cell in
            cell.textLabel?.text = item
        }
        
        menu.selectionHandler = { [unowned self, unowned menu] _, item in
            input.text = item
            self.textChanged()
            menu.dismiss(animated: true, completion: nil)
        }
        
        let rect = input.convert(input.moreButton.frame, to: view)
        menu.presentMenu(in: self, sourceRect: rect)
    }
}

extension NewGameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == newGameView.team1Input.input.field {
            newGameView.team2Input.input.field.becomeFirstResponder()
        } else {
            newGameView.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""

        guard let stringRange = Range(range, in: currentText) else { return false }

        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        return updatedText.count <= 12
    }
}
