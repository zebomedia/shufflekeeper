//
//  InputField.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class InputField: UIView {
    var moreHandler: (() -> Void)?
    var textChangeHandler: (() -> Void)?
    
    enum Constants {
        static let margins = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 4)
        static let cornerRadius: CGFloat = 8
        static let moreSize: CGFloat = 40
    }
    
    private lazy var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.field, self.moreButton])) {
        $0.isLayoutMarginsRelativeArrangement = true
        $0.layoutMargins = Constants.margins
        $0.axis = .horizontal
    }
    
    private(set) lazy var field: UITextField = self.configureSubview(UITextField()) {
        $0.autocapitalizationType = .words
        $0.returnKeyType = .next
        $0.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
    }
    
    private(set) lazy var moreButton: UIButton = self.configureSubview(UIButton(type: .system)) {
        $0.setImage(UIImage(systemName: "ellipsis"), for: .normal)
        $0.widthAnchor.constraint(equalToConstant: Constants.moreSize).activate()
        $0.setContentHuggingPriority(.required, for: .horizontal)
        $0.tintColor = UIColor.secondaryLabel
        $0.addTarget(self, action: #selector(self.morePressed), for: .touchUpInside)
    }
    
    var placeholder: String {
        get {
            return field.placeholder ?? ""
        }
        set {
            field.placeholder = newValue
        }
    }
    
    var text: String {
        get {
            return field.text ?? ""
        }
        set {
            field.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        constrainSubview(stackView, insets: UIEdgeInsets.zero)
        backgroundColor = UIColor.secondarySystemGroupedBackground
        layer.cornerRadius = Constants.cornerRadius
        
        moreButton.addTarget(self, action: #selector(morePressed), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func morePressed() {
        moreHandler?()
    }
    
    @objc func textChanged() {
        textChangeHandler?()
    }
}
