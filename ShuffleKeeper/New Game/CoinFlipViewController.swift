//
//  CoinFlipViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

final class CoinFlipViewController: UIViewController {
    enum Constants {
        static let coinSize: CGFloat = 150
        static let buttonMargin: CGFloat = 40
    }
    private let heads = UIImage(named: "heads")!
    private let tails = UIImage(named: "tails")!
    
    private lazy var imageView: UIImageView = self.view.configureSubview(UIImageView()) {
        $0.widthAnchor.constraint(equalToConstant: Constants.coinSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.coinSize).activate()
        $0.alpha = 0
    }
    
    private lazy var doneButton = self.view.configureSubview(UIButton.solidButton(title: "Done"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).activate()
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -60).activate()
        
        doneButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.buttonMargin).activate()
        doneButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.buttonMargin).activate()
        doneButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.buttonMargin).activate()
        
        doneButton.addTarget(self, action: #selector(donePressed), for: .touchUpInside)
        
        view.backgroundColor = UIColor.ShuffleKeeper.backgroundColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        perform(#selector(showRandomCoin), with: nil, afterDelay: 0.5)
    }
    
    @objc private func showRandomCoin() {
        imageView.image = [heads, tails].randomElement()
        
        UIView.animate(withDuration: 0.5) {
            self.imageView.alpha = 1.0
        }
    }
    
    @objc private func donePressed() {
        dismiss(animated: true, completion: nil)
    }
}
