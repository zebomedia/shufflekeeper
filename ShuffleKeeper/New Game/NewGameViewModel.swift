//
//  NewGameViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/22/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import Foundation

final class NewGameViewModel {
    private let teamList: [String]
    private var teamNames: [String] = []
    
    var storedTeamNames: [String] {
        return teamList.filter { !teamNames.contains($0) }
    }
    
    var navigationHandler: NavigationHandler?
    var updateHandler: (() -> Void)?
    
    var hideTeamChooser: Bool {
        return storedTeamNames.isEmpty
    }
    
    var teamsAreValid: Bool {
        for team in teamNames {
            guard !team.isEmpty else { return false }
        }
        return true
    }
    
    init(allGames: [Game]) {
        let teams: [Team] = allGames.flatMap { $0.teams }
        self.teamList = Set(teams)
            .map { $0.name }
            .sorted()
    }
    
    func setTeamNames(_ teams: [String]) {
        teamNames = teams
        
        updateHandler?()
    }
    
    func startGame(team1Name: String, team2Name: String) {
        navigationHandler?(.startGame(team1Name: team1Name,
                                      team2Name: team2Name))
    }
}
