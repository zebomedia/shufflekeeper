//
//  Assets.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/7/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

extension UIImage {
    static let yellowBiscuit = UIImage(named: "yellow biscuit")!
    static let blackBiscuit = UIImage(named: "black biscuit")!
}

extension UIColor {
    enum ShuffleKeeper {
        static let yellow = UIColor(named: "YellowTeam")!
        static let black = UIColor(named: "BlackTeam")!
        static let backgroundColor = UIColor(named: "background")!
        static let secondaryBackgroundColor = UIColor(named: "secondary background")!
    }
}
