//
//  GameHeaderTeamView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameHeaderTeamView: UIView {
    enum Constants {
        static let circleSize: CGFloat = 18
        static let dotSpacing: CGFloat = 6
    }
    
    private(set) lazy var name: UILabel = self.configureSubview(UILabel()) {
        $0.font = UIFont.preferredFont(forTextStyle: .headline)
        $0.textAlignment = .center
        $0.setContentHuggingPriority(.defaultLow, for: .vertical)
    }
    
    private(set) lazy var circle: CircleView = self.configureSubview(CircleView()) {
        $0.heightAnchor.constraint(equalToConstant: Constants.circleSize).activate()
        $0.widthAnchor.constraint(equalToConstant: Constants.circleSize).activate()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        name.centerYAnchor.constraint(equalTo: centerYAnchor).activate()
        name.centerXAnchor.constraint(equalTo: centerXAnchor).activate()
        circle.centerYAnchor.constraint(equalTo: centerYAnchor).activate()
        circle.trailingAnchor.constraint(equalTo: name.leadingAnchor, constant: -Constants.dotSpacing).activate()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
