//
//  GameFrameCellViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/22/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct GameFrameCellViewModel {
    let score1: String
    let score2: String
    let font: UIFont
    let textColor: UIColor
    let showIndicator1: Bool
    let showIndicator2: Bool
    let showBottomDivider: Bool
}
