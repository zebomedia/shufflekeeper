//
//  GameViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameViewModel {
    enum Constants {
        static let footerHeight: CGFloat = 100
        static let firstPattern = [0,1,1,0,0,1,1,0]
    }
    
    private(set) var game: Game
    
    private let numFrames = 8
    
    private let scoreFont = UIFont.preferredFont(forTextStyle: .body)
    private let totalFont = UIFont.preferredFont(forTextStyle: .title2).bold()
    
    private var rowHeight: CGFloat = 0
    
    var navigationHandler: NavigationHandler?
    var frameUpdateHandler: (() -> Void)?
    
    var headerViewModel: GameHeaderViewModel {
        return GameHeaderViewModel(team1Name: game.teams[0].name,
                                   team1Color: UIColor.ShuffleKeeper.yellow,
                                   team2Name: game.teams[1].name,
                                   team2Color: UIColor.ShuffleKeeper.black)
    }
    
    var title: String {
        return "\(game.teams[0].name) vs \(game.teams[1].name)"
    }
    
    var numberOfRows: Int {
        return numFrames + 2
    }
    
    var headerHeight: CGFloat {
        return rowHeight
    }
    
    var viewBounds: CGRect = .zero {
        didSet {
            rowHeight = floor(viewBounds.height/CGFloat(numberOfRows))
        }
    }
    
    init(game: Game) {
        self.game = game
    }
    
    func frameModel(at index: Int) -> GameFrameCellViewModel {
        guard index < numFrames else {
            return GameFrameCellViewModel(score1: "\(game.totalScore(forTeamAt: 0))",
                score2: "\(game.totalScore(forTeamAt: 1))",
                font: totalFont, textColor: UIColor.label, showIndicator1: false, showIndicator2: false, showBottomDivider: true)
        }
        
        let whosFirst = firstTeam(forFrame: index)
        let showIndicator1 = whosFirst == 0
        let showIndicator2 = whosFirst == 1

        let showDivider = index >= numFrames - 1
        guard index < game.frames.count else {
            return GameFrameCellViewModel(score1: "-", score2: "-", font: scoreFont, textColor: UIColor.secondaryLabel, showIndicator1: showIndicator1, showIndicator2: showIndicator2, showBottomDivider: showDivider)
        }
        
        let frame = game.frames[index]
        return GameFrameCellViewModel(score1: "\(frame.score(for: game.teams[0]))",
            score2: "\(frame.score(for: game.teams[1]))",
            font: scoreFont, textColor: UIColor.label, showIndicator1: showIndicator1, showIndicator2: showIndicator2, showBottomDivider: showDivider)
    }
    
    func height(forRowAt index: Int) -> CGFloat {
        return rowHeight
    }
    
    func addFrame() {
        navigationHandler?(NavigationCommand.showFrame(game: game,
                                                       frameNum: nil,
                                                       firstTeamIndex: Constants.firstPattern[game.frames.count]))
    }
    
    func showFrame(num: Int) {
        guard num < game.frames.count else {
            addFrame()
            return
        }
        
        navigationHandler?(NavigationCommand.showFrame(game: game,
                                                       frameNum: num,
                                                       firstTeamIndex: Constants.firstPattern[num]))
    }
    
    func save(frame: Frame) {
        var currentFrames = game.frames
        if frame.num == currentFrames.count {
            currentFrames.append(frame)
        } else {
            currentFrames[frame.num] = frame
        }
        
        game = Game(date: game.date, teams: game.teams, frames: currentFrames)
        frameUpdateHandler?()
    }
    
    private func firstTeam(forFrame num: Int) -> Int {
        return Constants.firstPattern[num]
    }
}
