//
//  GameFrameTeamScoreView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/23/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameFrameTeamScoreView: UIView {
    enum Constants {
        static let indicatorSize: CGFloat = 40
        static let dotColor = UIColor.systemRed
        static let leadingSpace: CGFloat = 10
        static let indicatorRadius: CGFloat = 8
    }
    
    lazy var label: UILabel = self.configureSubview(UILabel()) {
        $0.textAlignment = .center
    }
    
    lazy var turnIndicator: UIView = self.configureSubview(UIView()) {
        $0.layer.borderColor = UIColor.opaqueSeparator.resolvedColor(with: self.traitCollection).cgColor
        $0.layer.borderWidth = GlobalConstants.lineWidth
        $0.layer.cornerRadius = Constants.indicatorRadius
        $0.widthAnchor.constraint(equalToConstant: Constants.indicatorSize).activate()
        $0.heightAnchor.constraint(equalToConstant: Constants.indicatorSize).activate()
    }
    
    var showIndicator = false {
        didSet {
            turnIndicator.isHidden = !self.showIndicator
        }
    }
    
    init(position: Int) {
        super.init(frame: .zero)
        
        constrainSubview(label, insets: .zero)
        turnIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).activate()
        turnIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).activate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
