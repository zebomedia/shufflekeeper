//
//  GameFooterView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/31/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameFooterView: UIView {
    enum Constants {
        static let margin: CGFloat = 40
    }
    
    private(set) lazy var button = self.configureSubview(UIButton.solidButton(title: "Add Frame"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        button.centerYAnchor.constraint(equalTo: centerYAnchor).activate()
        button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.margin).activate()
        button.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.margin).activate()
        
        backgroundColor = UIColor.ShuffleKeeper.backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
