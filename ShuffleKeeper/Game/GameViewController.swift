//
//  GameViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameViewController: UIViewController {
    let viewModel: GameViewModel
    
    private var initialized = false
    
    private lazy var tableView: UITableView = self.view.configureSubview(UITableView())
    
    private lazy var footer: GameFooterView = self.view.configureSubview(GameFooterView()) {
        $0.heightAnchor.constraint(equalToConstant: GameViewModel.Constants.footerHeight).activate()
        $0.setContentHuggingPriority(.required, for: .vertical)
    }
    
    init(viewModel: GameViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.ShuffleKeeper.backgroundColor
        tableView.backgroundColor = UIColor.ShuffleKeeper.backgroundColor
        
        navigationItem.setRightBarButton(UIBarButtonItem(image: UIImage(systemName: "info.circle"),
                                                         style: .plain,
                                                         target: self,
                                                         action: #selector(showInfo)),
                                         animated: false)
        
        
        footer.button.addTarget(self, action: #selector(addFrame), for: .touchUpInside)
        
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        
        viewModel.frameUpdateHandler = { [weak self] in
            self?.tableView.reloadData()
        }
        
        view.leadingAnchor.constraint(equalTo: tableView.leadingAnchor).activate()
        view.trailingAnchor.constraint(equalTo: tableView.trailingAnchor).activate()
        view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: tableView.topAnchor).activate()
        view.leadingAnchor.constraint(equalTo: footer.leadingAnchor).activate()
        view.trailingAnchor.constraint(equalTo: footer.trailingAnchor).activate()
        view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: footer.bottomAnchor).activate()
        tableView.bottomAnchor.constraint(equalTo: footer.topAnchor).activate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !initialized {
            setup()
            initialized = true
        }
    }
    
    private func setup() {
        viewModel.viewBounds = tableView.bounds

        let tableHeader = GameHeaderView(viewModel: viewModel.headerViewModel,
                                         frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: viewModel.headerHeight))

        tableView.tableHeaderView = tableHeader
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: GameFrameCell.self)
    }
    
    @objc func showInfo() {
        
    }
    
    @objc func addFrame() {
        viewModel.addFrame()
    }
}

extension GameViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.frameModel(at: indexPath.row)
        let cell: GameFrameCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.configure(with: model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.height(forRowAt: indexPath.row)
    }
}

extension GameViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.showFrame(num: indexPath.row)
        tableView.selectRow(at: nil, animated: true, scrollPosition: .none)
    }
}

