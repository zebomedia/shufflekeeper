//
//  GameHeaderViewModel.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct GameHeaderViewModel {
    let team1Name: String
    let team1Color: UIColor
    let team2Name: String
    let team2Color: UIColor
}
