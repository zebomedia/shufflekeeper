//
//  GameFrameCell.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameFrameCell: UITableViewCell {
    enum Constants {
        static let dividerThickness: CGFloat = GlobalConstants.lineWidth
    }
    
    private lazy var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.team1, self.middleDivider, self.team2])) {
        $0.axis = .horizontal
        $0.spacing = 0
        $0.alignment = .fill
        $0.distribution = .fill
    }
    
    private(set) lazy var team1: GameFrameTeamScoreView = self.configureSubview(GameFrameTeamScoreView(position: 0))
    
    private(set) lazy var team2: GameFrameTeamScoreView = self.configureSubview(GameFrameTeamScoreView(position: 1))
    
    private lazy var middleDivider: UIView = self.configureSubview(UIView()) {
        $0.widthAnchor.constraint(equalToConstant: Constants.dividerThickness).activate()
        $0.backgroundColor = UIColor.opaqueSeparator
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    private lazy var bottomDivider: UIView = self.configureSubview(UIView()) {
        $0.heightAnchor.constraint(equalToConstant: Constants.dividerThickness).activate()
        $0.backgroundColor = UIColor.opaqueSeparator
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        constrainSubview(stackView, insets: UIEdgeInsets.zero)
        team1.widthAnchor.constraint(equalTo: team2.widthAnchor).activate()
        
        self.constrainSubview(bottomDivider, constraints: [equal(\.leadingAnchor, \.leadingAnchor),
                                                           equal(\.trailingAnchor, \.trailingAnchor),
                                                           equal(\.bottomAnchor, \.bottomAnchor)])
        backgroundColor = UIColor.ShuffleKeeper.backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension GameFrameCell {
    func configure(with viewModel: GameFrameCellViewModel) {
        team1.label.text = viewModel.score1
        team2.label.text = viewModel.score2
        team1.label.font = viewModel.font
        team1.label.textColor = viewModel.textColor
        team2.label.font = viewModel.font
        team2.label.textColor = viewModel.textColor
        team1.showIndicator = viewModel.showIndicator1
        team2.showIndicator = viewModel.showIndicator2
        bottomDivider.isHidden = !viewModel.showBottomDivider
    }
}
