//
//  GameHeaderView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class GameHeaderView: UIView {
    enum Constants {
        static let dividerThickness: CGFloat = GlobalConstants.lineWidth
    }
    
    private lazy var stackView: UIStackView = self.configureSubview(UIStackView(arrangedSubviews: [self.team1, self.middleDivider, self.team2])) {
        $0.axis = .horizontal
        $0.spacing = 0
        $0.alignment = .fill
        $0.distribution = .fill
    }
    
    private(set) lazy var team1: GameHeaderTeamView = self.configureSubview(GameHeaderTeamView())
    private(set) lazy var team2: GameHeaderTeamView = self.configureSubview(GameHeaderTeamView())
    
    private lazy var middleDivider: UIView = self.configureSubview(UIView()) {
        $0.widthAnchor.constraint(equalToConstant: Constants.dividerThickness).activate()
        $0.backgroundColor = UIColor.opaqueSeparator
        $0.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    private lazy var bottomDivider: UIView = self.configureSubview(UIView()) {
        $0.heightAnchor.constraint(equalToConstant: Constants.dividerThickness).activate()
        $0.backgroundColor = UIColor.opaqueSeparator
    }
    
    init(viewModel: GameHeaderViewModel, frame: CGRect) {
        super.init(frame: frame)
    
        team1.circle.color = viewModel.team1Color
        team1.name.text = viewModel.team1Name
        
        team2.circle.color = viewModel.team2Color
        team2.name.text = viewModel.team2Name
        
        constrainSubview(stackView, insets: UIEdgeInsets.zero)
        team1.widthAnchor.constraint(equalTo: team2.widthAnchor).activate()
        
        self.constrainSubview(bottomDivider, constraints: [equal(\.leadingAnchor, \.leadingAnchor),
                                                           equal(\.trailingAnchor, \.trailingAnchor),
                                                           equal(\.bottomAnchor, \.bottomAnchor)])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
