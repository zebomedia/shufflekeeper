//
//  AppCoordinator.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

enum NavigationCommand {
    case newGame
    case startGame(team1Name: String, team2Name: String)
    case showGame(game: Game)
    case showFrame(game: Game, frameNum: Int?, firstTeamIndex: Int)
}

typealias NavigationHandler = (NavigationCommand) -> Void

final class AppCoordinator {
    let window: UIWindow
    let navController: UINavigationController
    
    private let gamesListModel: GamesViewModel
    private var currentGameModel: GameViewModel?
    
    init(window: UIWindow) {
        self.window = window
        self.navController = UINavigationController()
        self.gamesListModel = GamesViewModel()
    }
    
    func start() {
        gamesListModel.navigationHandler = { [unowned self] command in
            self.handle(navigationCommand: command)
        }
        
        let gamesViewController = GamesViewController(viewModel: gamesListModel)
        navController.pushViewController(gamesViewController, animated: false)
        
        window.rootViewController = navController
    }
    
    private func handle(navigationCommand: NavigationCommand) {
        switch navigationCommand {
        case .newGame:
            let viewModel = NewGameViewModel(allGames: gamesListModel.games)
            
            viewModel.navigationHandler = { [unowned self] command in
                self.handle(navigationCommand: command)
            }
            
            let newGameViewController = NewGameViewController(viewModel: viewModel)
            let nav = UINavigationController(rootViewController: newGameViewController)
            
            navController.present(nav, animated: true, completion: nil)
            
        case .startGame(let team1Name, let team2Name):
            gamesListModel.addGame(team1: team1Name, team2: team2Name)
            
        case .showGame(let game):
            let viewModel = GameViewModel(game: game)
            
            viewModel.navigationHandler = { [unowned self] command in
                self.handle(navigationCommand: command)
            }
            
            let gameViewController = GameViewController(viewModel: viewModel)
            currentGameModel = viewModel
            
            navController.pushViewController(gameViewController, animated: true)
            
        case .showFrame(let game, let frameNum, let firstTeamIndex):
            let frame: Frame = {
                guard let index = frameNum else {
                    return Frame(num: game.frames.count, shots: [])
                }
                return game.frames[index]
            }()
            
            let viewModel = FrameScoreViewModel(teams: game.teams, frame: frame, firstTeamIndex: firstTeamIndex)
            
            let frameScoreViewController = FrameScoreViewController(viewModel: viewModel)
            let nav = UINavigationController(rootViewController: frameScoreViewController)
            
            viewModel.saveHandler = { [weak self] frame in
                guard let s = self, let currentGame = s.currentGameModel else { return }
                currentGame.save(frame: frame)
                s.gamesListModel.update(game: currentGame.game)
            }
            
            navController.present(nav, animated: true, completion: nil)
        }
    }
}
