//
//  Shot.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct Shot: Codable {
    let team: Team
    let shotType: ShotType
    let teamType: TeamType
}

extension Shot {
    var biscuit: UIImage {
        return teamType.biscuit
    }
}
