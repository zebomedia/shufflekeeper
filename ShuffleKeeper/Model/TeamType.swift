//
//  TeamType.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/7/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

enum TeamType: Int, Codable {
    case first
    case second
    
    var color: UIColor {
        switch self {
            case .first: return UIColor.ShuffleKeeper.yellow
            case .second: return UIColor.ShuffleKeeper.black
        }
    }
    
    var biscuit: UIImage {
        switch self {
            case .first: return UIImage.yellowBiscuit
            case .second: return UIImage.blackBiscuit
        }
    }
}

