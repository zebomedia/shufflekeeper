//
//  Frame.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import Foundation

struct Frame: Codable {
    let num: Int
    var shots: [Shot]
}

extension Frame {
    func score(for team: Team) -> Int {
        return shots
            .filter { $0.team == team }
            .reduce(0) { $0 + $1.shotType.value }
    }
    
    func shotCount(for teamType: TeamType) -> Int {
        return shots.filter { $0.teamType == teamType }.count
    }
}
