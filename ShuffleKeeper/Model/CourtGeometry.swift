//
//  CourtGeometry.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/8/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

struct CourtGeometry {
    
    enum Constants {
        static let tenPercent: CGFloat = 0.27
        static let eightPercent: CGFloat = 0.58
        static let kitchenPercent: CGFloat = 0.88
        static let eightWidthPercent: CGFloat = 0.71
        static let tenWidthPercent: CGFloat = 0.30
    }

    var tenHeight: CGFloat {
        return bounds.height * Constants.tenPercent
    }
    
    var eightHeight: CGFloat {
        return bounds.height * (Constants.eightPercent - Constants.tenPercent)
    }
    
    var sevenHeight: CGFloat {
        return bounds.height * (Constants.kitchenPercent - Constants.eightPercent)
    }
    
    var kitchenHeight: CGFloat {
        return bounds.height * (1.0 - Constants.kitchenPercent)
    }
    
    let bounds: CGRect
    
    func isOnCourt(point: CGPoint) -> Bool {
        return point.y > 0 && point.y < bounds.height
    }

    func getShotType(point: CGPoint) -> ShotType {
        let yPercent = point.y/bounds.height
        let xPercent = point.x/bounds.width
        
        let isLeft = xPercent < 0.5
        if yPercent < Constants.tenPercent {
            return .ten
        } else if yPercent < Constants.eightPercent {
            return isLeft ? .leftEight : .rightEight
        } else if yPercent < Constants.kitchenPercent {
            return isLeft ? .leftSeven : .rightSeven
        } else {
            return isLeft ? .leftKitchen : .rightKitchen
        }
    }

    func centerPoint(for shotType: ShotType) -> CGPoint {
        let midX = bounds.midX
        let tenWidth = bounds.width * Constants.tenWidthPercent * 0.5
        let eightWidth = bounds.width * Constants.eightWidthPercent * 0.5
        
        switch shotType {
            case .ten: return CGPoint(x: midX, y: tenHeight * 0.7)
            case .leftEight: return CGPoint(x: midX - tenWidth * 0.7, y: tenHeight + eightHeight * 0.6)
            case .rightEight: return CGPoint(x: midX + tenWidth * 0.7, y: tenHeight + eightHeight * 0.6)
            case .leftSeven: return CGPoint(x: midX - eightWidth * 0.5, y: Constants.eightPercent * bounds.height + sevenHeight * 0.5)
            case .rightSeven: return CGPoint(x: midX + eightWidth * 0.5, y: Constants.eightPercent * bounds.height + sevenHeight * 0.5)
            case .leftKitchen: return CGPoint(x: midX - midX * 0.45, y: bounds.height - kitchenHeight * 0.5)
            case .rightKitchen: return CGPoint(x: midX + midX * 0.45, y: bounds.height - kitchenHeight * 0.5)
        }
    }
    
    func getRects(for shots: [Shot], biscuitSize: CGFloat) -> [CGRect] {
        let groupedByType: [ShotType: [Shot]] = Dictionary(grouping: shots, by: { $0.shotType })
        
        var rectsByType: [ShotType: [CGRect]] = [:]
        for (type, shots) in groupedByType {
            rectsByType[type] = ShotPositions.getShotRects(shotType: type, count: shots.count, biscuitSize: biscuitSize, courtSize: bounds.size)
        }
        
        return shots.map {
            var rects = rectsByType[$0.shotType]!
            guard !rects.isEmpty else { return .zero }
            
            let rect = rects.removeFirst()
            rectsByType[$0.shotType] = rects
            
            return rect
        }
    }
}
