//
//  Team.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import Foundation

struct Team: Hashable, Codable {
    let name: String
}
