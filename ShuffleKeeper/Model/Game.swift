//
//  Game.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import Foundation

struct Game: Codable {
    let date: Date
    let teams: [Team]
    let frames: [Frame]
}

extension Game {
    func totalScore(forTeamAt index: Int) -> Int {
        return totalScore(for: teams[index])
    }
    
    func totalScore(for team: Team) -> Int {
        return frames.reduce(0) { $0 + $1.score(for: team) }
    }
}
