//
//  ShotPositions.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/21/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

fileprivate typealias ShotPosition = Array<CGFloat>

enum ShotPositions {
    static let actualBiscuitSize: CGFloat = 22
    fileprivate static let baseCourtSize = CGSize(width: 352, height: 550)
    fileprivate static let baseOffset = CGPoint(x: 31, y: 226)
    
    fileprivate static let tenPositions: [[ShotPosition]] = [
        [[194, 281]],
        [[194, 281], [194, 348]],
        [[194, 281], [167, 348], [225, 348]],
        [[194, 281], [167, 348], [194, 348], [225, 348]],
        [[194, 261], [194, 289], [167, 348], [194, 348], [225, 348]],
        [[194, 261], [194, 289], [167, 348], [194, 348], [225, 348], [175, 317]],
        [[194, 261], [194, 289], [167, 348], [194, 348], [225, 348], [175, 317], [215, 317]],
        [[194, 261], [194, 289], [167, 348], [194, 348], [225, 348], [175, 317], [215, 317], [194, 317]]
    ]
    
    fileprivate static let eightPositions: [[ShotPosition]] = [
        [[167, 408]],
        [[167, 408], [127, 509]],
        [[167, 408], [127, 509], [171, 509]],
        [[167, 408], [127, 509], [171, 509], [181, 457]],
        [[167, 408], [127, 509], [171, 509], [181, 457], [127, 457]],
        [[147, 408], [127, 509], [171, 509], [181, 457], [127, 457], [181, 408]],
        [[147, 408], [111, 509], [143, 509], [181, 457], [122, 457], [181, 408], [177, 509]],
        [[147, 408], [111, 509], [143, 509], [181, 457], [122, 457], [181, 408], [177, 509], [150, 483]]
    ]
    
    fileprivate static let sevenPositions: [[ShotPosition]] = [
        [[134, 576]],
        [[134, 576], [132, 665]],
        [[134, 576], [82, 665], [167, 665]],
        [[111, 576], [82, 665], [167, 665], [167, 576]],
        [[111, 576], [68, 665], [167, 665], [167, 576], [117, 665]],
        [[111, 576], [68, 665], [167, 665], [167, 576], [117, 665], [88, 618]],
        [[111, 576], [68, 665], [167, 665], [167, 576], [117, 665], [88, 618], [167, 618]],
        [[111, 586], [68, 665], [167, 665], [167, 586], [117, 665], [88, 621], [167, 621], [141, 556]]
    ]
    
    fileprivate static let kitchenPositions: [[ShotPosition]] = [
        [[74, 734]],
        [[74, 734], [161, 734]],
        [[55, 734], [82, 734], [164,734]],
        [[55, 734], [82, 734], [151,734], [179, 734]],
        [[55, 722], [82, 722], [151,722], [179, 722], [68,747]],
        [[55, 722], [82, 722], [151,722], [179, 722], [68,747], [165, 747]],
        [[55, 722], [82, 722], [151,722], [179, 722], [55,747], [165, 747], [82, 747]],
        [[55, 722], [82, 722], [151,722], [179, 722], [55,747], [151, 747], [82, 747], [179,747]]
    ]
    
    fileprivate static func getShotPositions(type: ShotType) -> [[ShotPosition]] {
        switch type {
            case .ten: return tenPositions
            case .leftEight: return eightPositions
            case .rightEight: return eightPositions.inverted()
            case .leftSeven: return sevenPositions
            case .rightSeven: return sevenPositions.inverted()
            case .leftKitchen: return kitchenPositions
            case .rightKitchen: return kitchenPositions.inverted()
        }
    }
    
    static func getShotRects(shotType: ShotType, count: Int, biscuitSize: CGFloat, courtSize: CGSize) -> [CGRect] {
        let allPositions = getShotPositions(type: shotType)
        guard allPositions.count > count - 1 else { return [] }
        
        let positions = allPositions[count - 1]
        
        return positions.map { $0.toRect(size: biscuitSize, courtSize: courtSize) }
    }
}

fileprivate extension Array where Element == [ShotPosition] {
    func inverted() -> [[ShotPosition]] {
        return self.map { $0.map
            {
                let adjustedX = $0[0] - ShotPositions.baseOffset.x
                let x = ShotPositions.baseCourtSize.width/2 - adjustedX - ShotPositions.actualBiscuitSize + ShotPositions.baseCourtSize.width/2 + ShotPositions.baseOffset.x
                return [x, $0[1]]
            }
        }
    }
}

extension ShotPosition {
    func toRect(size: CGFloat, courtSize: CGSize) -> CGRect {
        let diff = (size - ShotPositions.actualBiscuitSize)/2
        let xPercent: CGFloat = (self[0] - ShotPositions.baseOffset.x)/ShotPositions.baseCourtSize.width
        let yPercent: CGFloat = (self[1] - ShotPositions.baseOffset.y)/ShotPositions.baseCourtSize.height
        return CGRect(origin: CGPoint(x: xPercent * courtSize.width - diff,
                                      y: yPercent * courtSize.height - diff),
            size: CGSize(width: size, height: size)).integral
    }
}
