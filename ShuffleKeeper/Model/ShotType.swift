//
//  ShotType.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 11/24/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import Foundation

enum ShotType: Int, CaseIterable, Codable {
    case ten
    case leftEight
    case rightEight
    case leftSeven
    case rightSeven
    case leftKitchen
    case rightKitchen
    
    var value: Int {
        switch self {
        case .ten: return 10
        case .leftEight, .rightEight: return 8
        case .leftSeven, .rightSeven: return 7
        case .leftKitchen, .rightKitchen: return -10
        }
    }
    
    var description: String {
        switch self {
            case .ten: return "Ten"
            case .leftEight: return "Left Eight"
            case .rightEight: return "Right Eight"
            case .leftSeven: return "Left Seven"
            case .rightSeven: return "Right Seven"
            case .leftKitchen: return "Left Kitchen"
            case .rightKitchen: return "Right Kitchen"
        }
    }
    
    var labelText: String {
        switch self {
            case .ten: return "10"
            case .leftEight: return "8"
            case .rightEight: return "8"
            case .leftSeven: return "7"
            case .rightSeven: return "7"
            case .leftKitchen: return "10"
            case .rightKitchen: return "OFF"
        }
    }
}
