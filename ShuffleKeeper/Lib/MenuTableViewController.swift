//
//  MenuTableViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

class MenuTableViewController<T:Equatable>: SimpleTableViewController<T, MenuTableViewCell>, UIPopoverPresentationControllerDelegate {
    override init(items:[T], configurationBlock:@escaping (_ item: T, _ cell: MenuTableViewCell) -> Void) {
        super.init(items: items, configurationBlock: configurationBlock)
    }

    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension MenuTableViewController where T == String {
    func presentMenu(in hostController: UIViewController, sourceRect: CGRect, arrowDirections:UIPopoverArrowDirection = .any) {
        let size = CGSize(width: MenuTableViewCell.Constants.font.widestLength(items) + 40, height: 44.0 * CGFloat(items.count))
        presentAsPopover(inViewController: hostController, size: size, arrowDirections: arrowDirections, sourceRect: sourceRect, popoverDelegate: self)
    }
}
