//
//  ArrayTableViewDataSource.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

class ArrayTableViewDataSource<T:Equatable, U:UITableViewCell>: NSObject, UITableViewDataSource {
    private(set) var items: [T]
    
    typealias CellConfigBlock = (_ item: T, _ cell: U) -> Void

    private(set) unowned var tableView:UITableView
    private let cellIdentifier = "cellIdentifier"
    var configurationBlock: CellConfigBlock?
    
    init(tableView:UITableView, items: [T]) {
        self.tableView = tableView
        self.items = items
        super.init()

        tableView.register(U.self, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! U
        let item: T = itemForIndexPath(indexPath)
        
        configurationBlock?(item, cell)
        return cell
    }
    
    func itemForIndexPath(_ indexPath:IndexPath) -> T {
        return items[indexPath.row]
    }
    
    func indexPathForItem(_ item: T) -> IndexPath? {
        if let index = items.firstIndex(of: item) {
            return IndexPath(item: index, section: 0)
        }
        return nil
    }
    
    func updateItems(_ items:[T], shouldReload:Bool = true) {
        self.items = items
        if shouldReload {
            tableView.reloadData()
        }
    }
}
