//
//  MenuTableViewCell.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    enum Constants {
        static let font = UIFont.preferredFont(forTextStyle: .body)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        textLabel?.textColor = UIColor.secondaryLabel
        textLabel?.font = Constants.font
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
