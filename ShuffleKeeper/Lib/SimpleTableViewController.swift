//
//  SimpleTableViewController.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

class SimpleTableViewController<T: Equatable, U: UITableViewCell>: UITableViewController {
    private(set) var dataSource: ArrayTableViewDataSource<T, U>?
    var selectionHandler: ((Int, T) -> Void)?
    var selectedIndex:Int?
    var selectedIndices:[Int]?
    var items: [T] {
        return dataSource?.items ?? []
    }
    
    init(items:[T], configurationBlock:@escaping (_ item: T, _ cell: U) -> Void) {
        super.init(nibName: nil, bundle: nil)
        self.dataSource = ArrayTableViewDataSource<T,U>(tableView: tableView, items: items)
        dataSource?.configurationBlock = configurationBlock
    }

    required init!(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let index = selectedIndex {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
        } else if let indices = selectedIndices {
            tableView.allowsMultipleSelection = true
            indices.forEach {
                tableView.selectRow(at: IndexPath(row: $0, section: 0), animated: false, scrollPosition: .none)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !tableView.allowsMultipleSelection {
            selectedIndex = indexPath.row
        }
        
        guard let item = dataSource?.items[indexPath.row] else { return }

        DispatchQueue.main.async {
            self.selectionHandler?(indexPath.row, item)
        }
    }

}
