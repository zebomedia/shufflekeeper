//
//  UIFont+Utilities.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/31/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

extension UIFont {
    func widthOfString(_ string: NSString) -> CGFloat {
        return ceil(string.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: self],
            context: nil).size.width)
    }
    
    func widestLength(_ strings:[String]) -> CGFloat {
        var widest: CGFloat = 0
        for str in strings {
            widest = max(widthOfString(str as NSString), widest)
        }
        return widest
    }
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }

    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }

    func italic() -> UIFont {
        return withTraits(traits: .traitItalic)
    }
}
