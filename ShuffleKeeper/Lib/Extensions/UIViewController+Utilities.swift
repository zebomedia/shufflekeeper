//
//  UIViewController+Utilities.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 1/25/20.
//  Copyright © 2020 Zebomedia. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentAsPopover(inViewController viewController: UIViewController,
                          size: CGSize,
                          arrowDirections: UIPopoverArrowDirection,
                          sourceRect: CGRect = CGRect.zero,
                          barButtonItem: UIBarButtonItem? = nil,
                          popoverDelegate: UIPopoverPresentationControllerDelegate? = nil,
                          backgroundColor: UIColor = UIColor.ShuffleKeeper.backgroundColor) {
        modalPresentationStyle = .popover
        preferredContentSize = size
        if let popoverController = popoverPresentationController {
            popoverController.backgroundColor = backgroundColor
            if let barButtonItem = barButtonItem {
                popoverController.barButtonItem = barButtonItem
            } else {
                popoverController.sourceView = viewController.view
                popoverController.sourceRect = sourceRect
            }
            popoverController.permittedArrowDirections = arrowDirections
            popoverController.delegate = popoverDelegate
        }
        // When a popover is presented, it should be safe to end editing in sub-views
        viewController.view.endEditing(true)
        viewController.present(self, animated: true, completion: nil)
    }
}
