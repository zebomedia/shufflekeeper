//
//  UIButton+Utilities.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/31/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

extension UIButton {
    static func solidButton(title: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.systemGray6, for: .normal)
        button.backgroundColor = UIColor.systemBlue
        button.layer.cornerRadius = 8
        button.heightAnchor.constraint(equalToConstant: 50).activate()
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body).bold()
        return button
    }
}
