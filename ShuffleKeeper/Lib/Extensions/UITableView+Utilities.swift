//
//  UITableView+Utilities.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 5/28/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(cellType: T.Type) {
        register(cellType, forCellReuseIdentifier: String(describing: cellType))
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath, cellType: T.Type = T.self) -> T {
        return self.dequeueReusableCell(withIdentifier: String(describing: cellType), for: indexPath) as! T
    }
    
    func register<T: UITableViewHeaderFooterView>(headerFooterViewType: T.Type) {
        self.register(headerFooterViewType.self, forHeaderFooterViewReuseIdentifier: String(describing: headerFooterViewType))
    }
    
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ viewType: T.Type = T.self) -> T {
        return self.dequeueReusableHeaderFooterView(withIdentifier: String(describing: viewType)) as! T
    }
}
