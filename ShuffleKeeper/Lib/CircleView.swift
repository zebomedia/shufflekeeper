//
//  CircleView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/31/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

class CircleView: UIView {
    private var shapeLayer: CAShapeLayer?
    
    var color: UIColor = UIColor.white {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var borderColor: UIColor? = nil {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect)
    {
        shapeLayer?.removeFromSuperlayer()
        let shape = CAShapeLayer()
        shape.fillColor = color.cgColor
        if let borderColor = borderColor, borderWidth > 0 {
            shape.strokeColor = borderColor.cgColor
            shape.lineWidth = borderWidth
        } else {
            shape.lineWidth = 0.0
        }
        layer.addSublayer(shape)
        shapeLayer = shape
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        shapeLayer?.path = UIBezierPath(ovalIn: bounds).cgPath
    }

}

