//
//  SizingStackView.swift
//  ShuffleKeeper
//
//  Created by Eric Zelermyer on 12/8/19.
//  Copyright © 2019 Zebomedia. All rights reserved.
//

import UIKit

final class SizingStackView: UIStackView {
    var sizingHandler: (() -> Void)?
    
    private var initialized = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !initialized {
            sizingHandler?()
            initialized = true
        }
    }
}
